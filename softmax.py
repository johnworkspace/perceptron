#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 25 22:39:15 2018

@author: JonatanHazel
"""

import numpy as np

# Write a function that takes as input a list of numbers, and returns
# the list of values given by the softmax function.
def softmax(L):
    softmaxList = []
    expL = np.exp(L)
    
    for l in expL:
        softmaxList.append(l/sum(np.exp(L)))
    return softmaxList
    