#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 25 18:18:10 2018

@author: JonatanHazel
"""

import matplotlib.pyplot as plt
file = open("data.csv","r")

data = []

for dataLine in file:
    data.append((dataLine.rstrip().split(',')))
    
print(data)

colors=['blue','red']

for point in data:
    plt.plot(float(point[0]),float(point[1]),marker='o',color=colors[int(point[2])])
    
