#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 25 21:43:01 2018

@author: JonatanHazel
"""

import numpy as np

def cross_entropy(Y, P):
    cross_entropy = 0
    for pi,yi in zip(P,Y):
        cross_entropy -= (np.log(pi) * yi + (np.log(1-pi)*(1-yi)))
    return cross_entropy

#Another solution
#    Y = np.float_(Y)
#    P = np.float_(P)
#    return -np.sum(Y * np.log(P) + (1 - Y) * np.log(1 - P))


def multi_class_cross_entropy(Y,P):
    cross_entropy = 0
    
    for pi,yi in zip(P,Y):
        for pij,yij in zip(pi,yi):
            cross_entropy -= np.log(pij)*yij
            
    return cross_entropy
    
    
